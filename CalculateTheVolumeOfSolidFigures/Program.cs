﻿using CalculateTheVolumeOfSolidFigures.ConsoleHelpers;
using System;
using UnitCalculator;
using Shapes.BusinessLogic;
using Shapes.BusinessLogic.Prisms;
namespace CalculateTheVolumeOfSolidFigures
{
    class Program
    {
        enum menuOptions
        {
            GetBallVolume=1,
            GetPrism4Volume=2,
            GetPrism3Volume=3,
            Exit=4,
        }

        static void Main(string[] args)
        {
            double a;
            double b;
            double c;
            double h;
            double r;

            var communicationhelper = new UserComunicationHelper();
            var menu = new Menu(typeof(menuOptions));           
            var menuunit = new Menu(typeof(DistanceUnit));

            menuOptions choice;
            DistanceUnit inputunit;
            DistanceUnit outputunit;

            do
            {
                choice = (menuOptions)menu.AskUser();
                var unitConversion = new UnitConverter();
                switch (choice)
                {
                    case menuOptions.GetBallVolume:                      
                        Console.WriteLine("input unit");
                        inputunit = (DistanceUnit)menuunit.AskUser();
                        r = communicationhelper.ReadNonNegativeDouble("r");
                        outputunit = DistanceUnit.Decimeter;
                        r=unitConversion.ConvertUnit(inputunit, outputunit, r);
                        var ball = new Ball(r);
                        Console.WriteLine($"V={ball.GetVolume()}litres");
                        break;

                    case menuOptions.GetPrism4Volume:
                        Console.WriteLine("input unit");
                        inputunit = (DistanceUnit)menuunit.AskUser();
                        outputunit = DistanceUnit.Decimeter;
                        a = communicationhelper.ReadNonNegativeDouble("a");
                        a = unitConversion.ConvertUnit(inputunit, outputunit, a);
                        b = communicationhelper.ReadNonNegativeDouble("b");
                        b = unitConversion.ConvertUnit(inputunit, outputunit, b);
                        c = communicationhelper.ReadNonNegativeDouble("c");
                        c = unitConversion.ConvertUnit(inputunit, outputunit, c);                    
                        var prism = new Prism4(a, b, c);
                        Console.WriteLine($"V={prism.GetVolume()}litres");
                        break;

                    case menuOptions.GetPrism3Volume:
                        Console.WriteLine("input unit");
                        inputunit = (DistanceUnit)menuunit.AskUser(); 
                        outputunit = DistanceUnit.Decimeter;
                        a = communicationhelper.ReadNonNegativeDouble("a");
                        a = unitConversion.ConvertUnit(inputunit, outputunit, a);
                        b = communicationhelper.ReadNonNegativeDouble("b");
                        b = unitConversion.ConvertUnit(inputunit, outputunit, b);
                        c = communicationhelper.ReadNonNegativeDouble("c");
                        c = unitConversion.ConvertUnit(inputunit, outputunit, c);
                        h = communicationhelper.ReadNonNegativeDouble("h");
                        h = unitConversion.ConvertUnit(inputunit, outputunit, h);
                        var prism3 = new Prism3(a,b,c,h);
                        Console.WriteLine($"V={prism3.GetVolume()}litres");
                        break;

                    case menuOptions.Exit:
                        break;

                    default:
                        throw new Exception($"unknown value:{choice}");
                }
            } while (choice != menuOptions.Exit);

        }
    }
}
