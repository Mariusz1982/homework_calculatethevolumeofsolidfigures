﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CalculateTheVolumeOfSolidFigures.ConsoleHelpers
{
    class Menu
    {

        private readonly Type _enumType;
        public Menu(Type enumType)//first char of parameter should be small
        {
            if (!enumType.IsEnum)//If not enum throw exception
            {
                throw new ArgumentException("Parameter is not enum");
            }
            _enumType = enumType;
        }

         public Enum AskUser()
        {

            foreach (var option in Enum.GetValues(_enumType))
            {
                Console.WriteLine($"{(int)option}-{option}");
            }

            string userInput;
            int choseOption;
            do
            {
                Console.WriteLine("Chose option:");
                userInput = Console.ReadLine();
            } while (!int.TryParse(userInput, out choseOption) ||
              !Enum.IsDefined(_enumType, choseOption));

            return (Enum)Enum.ToObject(_enumType, choseOption);
           
        }
    
    }
}
