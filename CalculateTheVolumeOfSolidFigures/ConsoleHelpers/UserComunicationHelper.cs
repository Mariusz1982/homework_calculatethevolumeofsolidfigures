﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CalculateTheVolumeOfSolidFigures.ConsoleHelpers
{
    class UserComunicationHelper
    {
        public double ReadNonNegativeDouble(string name)
        {

            string value;
            double result;        
            do
            {
                Console.Write($"{name}=");
                value = Console.ReadLine();
                if (value == null)
                {
                    throw new Exception("is empty-null");
                }
                      
            } while (!Double.TryParse(value, out result)|| result < 0);//while(if sign is not number or number is less than 0)
            return result;
            
        }
    }
}
