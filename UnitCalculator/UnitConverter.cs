﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnitCalculator
{
    public class UnitConverter
    {

        public double ConvertUnit(DistanceUnit inunit , DistanceUnit outunit, double number)//inunit is type of DistanceUnit
        {

            Double temporaryValue;
            switch (inunit)
            {
                case DistanceUnit.Meter:
                    temporaryValue = number;   
                    break;
                case DistanceUnit.Centimeter:
                    temporaryValue=number/ 100.0;
                    break;
                case DistanceUnit.Kilometer:
                    temporaryValue = number * 1000.0;
                    break;
                case DistanceUnit.NauticalMile:
                    temporaryValue = number * 1852.0;
                    break;
                case DistanceUnit.Mile:
                    temporaryValue = number * 1609.0;
                    break;
                case DistanceUnit.Yard:
                    temporaryValue = number * 0.9144;
                    break;
                case DistanceUnit.Decimeter:
                    temporaryValue = number/10;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("inunit", "out of range");        
            }

            switch (outunit)
            {
                case DistanceUnit.Meter:
                    break;
                case DistanceUnit.Kilometer:
                    temporaryValue=temporaryValue /1000.0;
                    break;
                case DistanceUnit.Mile:
                    temporaryValue = temporaryValue / 1609;
                    break;
                case DistanceUnit.NauticalMile:
                    temporaryValue = temporaryValue / 1852;
                    break;
                case DistanceUnit.Centimeter:
                    temporaryValue = temporaryValue * 100;
                    break;
                case DistanceUnit.Yard:
                    temporaryValue = temporaryValue / 0.9144;
                    break;
                case DistanceUnit.Decimeter:
                    temporaryValue = temporaryValue * 10;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("inunit", "out of range");
            }
            return temporaryValue;

        }

    }
}
