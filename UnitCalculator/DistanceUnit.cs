﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace UnitCalculator
{

    public enum DistanceUnit
        {   
            Meter,
            Centimeter,
            Kilometer,
            NauticalMile,
            Mile,
            Yard,
            Decimeter,
        }  
    
}
