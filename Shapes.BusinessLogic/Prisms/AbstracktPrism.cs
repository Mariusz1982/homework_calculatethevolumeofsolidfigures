﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.BusinessLogic.Prisms
{
    public abstract class AbstracktPrism:Shape
    {

        public AbstracktPrism(double h)
        {
            CheckIfNonNegative(h);
            H = h;
        }    

        public double H { get;  }
        public override sealed double GetVolume()
        {
            return GetBaseArea() * H;
        }

        public abstract double GetBaseArea();

    }
}
