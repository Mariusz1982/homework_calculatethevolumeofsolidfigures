﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.BusinessLogic.Prisms
{
    public class Prism4 : AbstracktPrism
    {

        public Prism4(double a, double b, double c)
            :base(c)//get and send to base constructor
        {
            CheckIfNonNegative(a, b);
            //if (a < 0||b<0||c<0)
            //throw new ArgumentException("prism4 edge can't be negative")
            A = a;
            B = b;
        }
        
        public double A { get; }//We get a, field is private
        public double B { get; }
        public override double GetBaseArea()
        {
            return A * B;
        }

    }
}
