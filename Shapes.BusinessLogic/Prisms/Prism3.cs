﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.BusinessLogic.Prisms
{
    public class Prism3:AbstracktPrism
    {
        public Prism3(double a, double b, double c, double h) : base(h)//h is sent to constructor of base class
        {
            
            CheckIfNonNegative(a, b, c);
            A = a;
            B = b;
            C = c;
           
            double[] triangleedges = { a, b, c };
            Array.Sort(triangleedges);// sort of length of triangle edges
            if (triangleedges[0]+ triangleedges[1]<=triangleedges[2])//we check conditions - if it triangle
            {
                throw new ArgumentException("a,b,c don't make a triangle");
            }

        }

        public double A { get; }
        public double B { get; }
        public double C { get; }
       
        public override double GetBaseArea()
        {
            double p = 0.5 * (A + B + C);
            return Math.Sqrt(p*(p-A)*(p-B)*(p-C));
        }
       
    }
}
