﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Shapes.BusinessLogic
{
    public abstract class Shape//abstract class Shape
    {

        public abstract double GetVolume();
        protected void CheckIfNonNegative(params double[] values)//we can give many values, it is array
        {
            if (values.Any() && values.Min()<0) // if value less than 0 throw exception
            {
                throw new ArgumentException();
            }
        }

    }
}
