﻿using System;


namespace Shapes.BusinessLogic
{  
    public class Ball:Shape
    {
        
        //readonly mean it can write/change only inside constructor
        public Ball(double radius)
        {
            CheckIfNonNegative(radius);// Ball inherit method from shape
            Radius = radius;//radius is set inside constructor
        }    

        public double Radius { get; }//we get radius, field is private
        public override double GetVolume()//ovveride because inherit method from shape
        {
            return (double)4/3 * Math.PI * Math.Pow(Radius,3);
        }

    }
}
